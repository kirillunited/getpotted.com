$(function () {
    $('body').on('click', '.js-bali-logo-title', function () {
        $(this).toggleClass('titled');
    });
    $(document).click(function (e) {
        if (!$('.js-bali-logo-title').is(e.target)) {
            $('.js-bali-logo-title').removeClass('titled');
        };
    });
});