
; /* Start:"a:4:{s:4:"full";s:98:"/local/templates/procashpo/components/bitrix/catalog.element/.default_new/script.js?15230175334611";s:6:"source";s:83:"/local/templates/procashpo/components/bitrix/catalog.element/.default_new/script.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/

$(document).ready(function() {
    $("body").on("click", ".video-preview", function() {
        $(this).hide();
        var $video = $(this).siblings('iframe'),
            src = $video.attr('src');

        $video.attr('src', src + '?rel=0&autoplay=1');
        return false
    });
});

$(document).ready(function(){


$('body').on('click', "#buy_item_block span.in_basket", function () {
	window.location.replace("/basket/");
});

$(document).on('keyup',function(evt) {
   if (evt.keyCode == 27) {
      $(".card_new-slaider-modal").hide();
   }
});


if($(".card_new-for-slaider_item").length > 3){
	
		$('.card_new-main-slaider_wrap').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  infinite: true,
		  adaptiveHeight: true,
		  asNavFor: '.card_new-for-slaider_wrap'
		});
	  	$('.card_new-for-slaider_wrap').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  asNavFor: '.card_new-main-slaider_wrap',
		  variableWidth: true,
		  infinite: true,
		  focusOnSelect: true
		});
	}
	else{

		$('.card_new-main-slaider_wrap').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  infinite: true,
		  adaptiveHeight: true
		});

		$('.card_new-for-slaider_wrap').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  centerMode: false,
		  variableWidth: true,
		  arrow: false,
		  dots: false,
		  infinite: false,
		  focusOnSelect: false,
		  swipe: false
		});
		$('.card_new-for-slaider_item').removeClass('slick-current');
		$(".card_new-for-slaider_wrap .slick-arrow").hide();
		$("body").on("click", ".slick-track .card_new-for-slaider_item", function() {
	        var moveSlide = $(this).data("slick-index");
	        var currentSlide = $('.card_new-main-slaider_wrap').slick('slickCurrentSlide');
	        if (moveSlide !== currentSlide) {
	          $('.card_new-main-slaider_wrap').slick('slickGoTo', moveSlide);
	        }
	    });
	}

	
	
	var modal = document.getElementById('slaider-modal');
	$('body').on('click', '.card_new-main-slaider_item.slick-active' ,function(){
		modal.style.display = "block";
			$("body").addClass("opened-big-modal");
			return false;
	});
	$('body').on('click', '#close-slaider-modal' ,function(){
		modal.style.display = "none";
		$("body").removeClass("opened-big-modal");
        return false;
	});

	jQuery(function($){
	$(document).mouseup(function (e){ 
		var div = $(".card_new-slaider-modal_modal-content"); 
			if (!div.is(e.target) 
		    	&& div.has(e.target).length === 0) {
				modal.style.display = "none";
			$("body").removeClass("opened-big-modal");
			}
		});
	});

	if($(window).width() <= 1001) {
		modal.style.display = "none";
		$("body").removeClass("opened-big-modal");
	}
	$( window ).resize(function() {
		if($(window).width() <= 1001) {
			modal.style.display = "none";
			$("body").removeClass("opened-big-modal");
		}
	});
	
	
	
	
	
$("body").on("click", "#open-slaider-modal", function() {

	var arr = [];
	$('.card_new-main-slaider').find('a').each(function(index) {
		if ($(this).attr('href') === '') {
			var video = $(this).find('iframe').attr('src');
			arr.push({href: video, type: 'iframe'});
		} else {
			arr.push($(this).attr('href'));
		}
	});

	$.fancybox.open( arr, {
		index : $(".card_new-main-slaider").find(".slick-active").index()
	});
	return false;
});
	/*$("body").on("click", "#open-slaider-modal", function() {
		$.fancybox.open($(".card_new-main-slaider").find(".slick-active"));
		return false;
	});
	*/
	
	/*var modal = document.getElementById('slaider-modal');
	var btn = document.getElementById("open-slaider-modal");
	var span = document.getElementById("close-slaider-modal");
	$('body').on('click', '#open-slaider-modal' ,function(){
		modal.style.display = "block";
			$("body").addClass("opened-big-modal");
	});
	$('body').on('click', '#close-slaider-modal' ,function(){
		modal.style.display = "none";
		$("body").removeClass("opened-big-modal");
	});
	
	jQuery(function($){
	$(document).mouseup(function (e){ 
		var div = $(".card_new-slaider-modal_modal-content"); 
			if (!div.is(e.target) 
		    	&& div.has(e.target).length === 0) {
				modal.style.display = "none";
			$("body").removeClass("opened-big-modal");
			}
		});
	});

	if($(window).width() <= 1001) {
		modal.style.display = "none";
		$("body").removeClass("opened-big-modal");
	}
	$( window ).resize(function() {
		if($(window).width() <= 1001) {
			modal.style.display = "none";
			$("body").removeClass("opened-big-modal");
		}
	});*/

});
/* End */
;
; /* Start:"a:4:{s:4:"full";s:93:"/local/templates/procashpo/components/custom/catalog.section/relative/script.js?1521180921714";s:6:"source";s:79:"/local/templates/procashpo/components/custom/catalog.section/relative/script.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
$(document).ready(function(){ 

		var viewedLength = $(".recently_viewed .product-page_viewed_wrap .item").length;
	
		if (viewedLength > 5) {
			$('.recently_viewed .product-page_viewed_wrap').owlCarousel({
			    loop:true,
			    margin:0,
			    nav:true,
			    autoheight: true,
			    responsive:{
			        0:{
			            items:1
			        },
			        580:{
			        	items:2
			        },
			        768:{
			            items:3
			        },
			     	980:{
			        	items:4
			        },
			        1200:{
			            items:5
			        }
			    }
			})
		}
		else{
			$(".recently_viewed .product-page_viewed_wrap .item").addClass("viewed-items");
		}
});
/* End */
;; /* /local/templates/procashpo/components/bitrix/catalog.element/.default_new/script.js?15230175334611*/
; /* /local/templates/procashpo/components/custom/catalog.section/relative/script.js?1521180921714*/
