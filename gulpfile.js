var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');
var gulpif = require('gulp-if');

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/*.{png,jpg}').pipe(spritesmith({
        imgName: '../../local/templates/procashpo/images/sprite-new.png',
        cssName: 'sprite.css'
    }));
    return spriteData.pipe(gulpif('*.png', gulp.dest('build/getpotted.com/local/templates/procashpo/images'), gulp.dest('build/getpotted.com/assets/css')));
});